const express = require("express");

const router = express.Router();

const keycloak = require("../config/keycloak-config.js").getKeycloak();

router.get("/anonymous", function (req, res) {
  res.send("Hello anonymous");
});

router.get("/user", keycloak.protect("user"), function (req, res) {
  res.send("Hello user");
});

router.get("/admin", keycloak.protect("admin"), function (req, res) {
  res.send({ message: "Hello admin", role: "admin", producto: req.query.producto, precio: req.query.precio });

  var mysql = require("mysql");

  var connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "seguridad-tp3",
    port: 3306,
  });

  connection.connect(function (error) {
    if (error) {
      throw error;
    } else {
      console.log("Conexion correcta.");
    }
  });

  try {
    var query = connection.query("INSERT INTO product VALUES(?, ?, ?)", ["null", "Gaseosa Serenito", 3000], function (error, result) {});
  } catch (ex) {
    connection.rollback();
    console.log(ex);
    throw error;
  }

  connection.end();
});

router.get("/all-user", keycloak.protect(["user", "admin"]), function (req, res) {
  res.send("Hello all user");
});

module.exports = router;
